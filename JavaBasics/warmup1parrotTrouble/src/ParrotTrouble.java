public class ParrotTrouble {
    public static void main(String[] args) {
        boolean s  = warmup1parrotTrouble(true, 7);
        System.out.println(s);
    }
    private static boolean warmup1parrotTrouble(boolean talking, int hour) {
        return (talking && (hour < 7 || hour > 20));
    }
}
