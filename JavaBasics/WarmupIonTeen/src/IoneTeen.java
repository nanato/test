public class IoneTeen {
    public static void main(String[] args) {
        boolean s  = ioneTeen(13, 99);
        System.out.println(s);
    }
    private static boolean ioneTeen(int a, int b) {
        if ((a >= 13 && a <= 19) && (b >= 13 && b <=19)) {
            return false;
        }
        return ((a >= 13 && a <= 19) || (b >= 13 && b <=19));
    }

}
